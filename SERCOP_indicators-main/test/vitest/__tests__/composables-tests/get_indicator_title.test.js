import { describe, expect, it } from 'vitest';
import getCurrentIndicatorTitle from 'src/composables/get_indicator_title';

describe('getCurrentIndicatorTitle', () => {
  it('returns the title for a given indicator', () => {
    expect(getCurrentIndicatorTitle('indicator_1_2'))
      .toBe('Porcentaje de registros con información incompleta');
    expect(getCurrentIndicatorTitle('indicator_3_4'))
      .toBe('Porcentaje de registros con información incompleta por tipo de procedimiento de contratación en la fase de adjudicación');
    expect(getCurrentIndicatorTitle('indicator_5_6'))
      .toBe('Ranking de entidades contratantes con mayor cantidad de registros incompletos');
    expect(getCurrentIndicatorTitle('indicator_7_8'))
      .toBe('Ranking de entidades contratantes con mayor cantidad de registros incompletos por procedimiento de contratación');
    expect(getCurrentIndicatorTitle('indicator_9'))
      .toBe('Contraste de registros de fecha de adjudicación vs fecha actual medida en días');
    expect(getCurrentIndicatorTitle('indicator_10_11'))
      .toBe('Número de registros con datos de montos $0 y $0,5');
    expect(getCurrentIndicatorTitle('indicator_12'))
      .toBe('Ranking de proveedores por mayor cantidad de registros con monto < $0,5 en la fase de licitación');
    expect(getCurrentIndicatorTitle('indicator_13'))
      .toBe('Ranking de entidades contratantes por mayor cantidad de registros con monto < $0,5 en la fase de adjudicación');
    expect(getCurrentIndicatorTitle('indicator_14'))
      .toBe('Registros con diferencias entre el monto de adjudicación y monto corregido por tipo de procedimiento de contratación');
    expect(getCurrentIndicatorTitle('indicator_15'))
      .toBe('Variabilidad de monto corregido entre monto de adjudicación y monto corregido de adjudicación');
    expect(getCurrentIndicatorTitle('indicator_16'))
      .toBe('Box plot de monto licitado por tipo de procedimiento de contratación pública');
    expect(getCurrentIndicatorTitle('indicator_17'))
      .toBe('Box plot de monto adjudicado por tipo de procedimiento de contratación pública');
  });

  it('returns undefined for an unknown indicator', () => {
    expect(getCurrentIndicatorTitle('indicator_unknown'))
      .toBe(undefined);
  });

})
