import { describe, expect, it } from 'vitest';
import getIndicatorDescription from 'src/composables/get_indicator_description';

const descriptions = {
  indicator_1:  'Total de registro que no cuentan con información de monto, en la plataforma de contrataciones abiertas; expresando como porcentaje del total de registros, por fases, de la plataforma de contrataciones abiertas, en un período determinado.',
  indicator_2:  'Total de registro que no cuentan con información de fecha, en la plataforma de contrataciones abiertas; expresado como porcentaje del total de registros, por fases, de la plataforma de contrataciones abiertas, en un  período determinado.',
  indicator_3:  'Total de registros que no cuentan con información de monto, por tipo de procedimiento de contratación; expresado como porcentaje del total de registros por tipo de procedimiento de contratación, en un periodo determinado.',
  indicator_4:  'Total de registros que no cuentan con información de fecha, por tipo de procedimiento de contratación; expresado como porcentaje del total de registros por tipo de procedimiento de contratación, en un periodo determinado.',
  indicator_5:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en monto, por fases del proceso de contratación.',
  indicator_6:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en fecha, por fases del proceso de contratación.',
  indicator_7:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en monto, por fases de proceso de contratación y procedimiento de contratación.',
  indicator_8:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en fecha, por fases del proceso de contratación y procedimiento de contratación.',
  indicator_9:  'Total de registro de adjudicación donde la diferencia entre la fecha de adjudicación y la fecha actual es mayor a 30 días; expresado como porcentaje del total de registros de adjudicación, en un periodo determinado.',
  indicator_10: 'Sumatoria de los procesos de contratación pública con montos de licitación cercanos a cero.',
  indicator_11: 'Sumatoria de los procesos de contratación publica con montos de adjudicación cercanos a cero.',
  indicator_12: 'Listado de los 10 proveedores con mayor cantidad de registros con monto < $0,5 en fase de licitación.',
  indicator_13: 'Listado de las 10 entidades contratantes con mayor cantidad de registros con montos < $0,5 en la fase de adjudicación.',
  indicator_14: 'Sumatoria de los registros que presentan cambios entre los montos de adjudicación y monto corregido de adjudicación, por tipo de procedimiento de contratación.',
  indicator_15: 'Variabilidad de monto corregido entre monto de adjudicación y monto corregido de adjudicación.',
  indicator_16: 'Gráfico de caja y bigotes para la variable monto licitado, por tipo de procedimiento contratación publica.',
  indicator_17: 'Box plot de monto adjudicado por tipo de procedimiento de contratación pública.',
};

describe('getIndicatorDescription', () => {
  it('returns the correct description for indicator_1_2 when toggleSelectedValue is "monto"', () => {
    const indicator = 'indicator_1_2';
    const toggleSelectedValue = 'monto';
    const expectedDescription = descriptions['indicator_1'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_1_2 when toggleSelectedValue is "fecha"', () => {
    const indicator = 'indicator_1_2';
    const toggleSelectedValue = 'fecha';
    const expectedDescription = descriptions['indicator_2'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_3_4 when toggleSelectedValue is "monto"', () => {
    const indicator = 'indicator_3_4';
    const toggleSelectedValue = 'monto';
    const expectedDescription = descriptions['indicator_3'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_3_4 when toggleSelectedValue is "fecha"', () => {
    const indicator = 'indicator_3_4';
    const toggleSelectedValue = 'fecha';
    const expectedDescription = descriptions['indicator_4'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_5_6 when toggleSelectedValue is "monto"', () => {
    const indicator = 'indicator_5_6';
    const toggleSelectedValue = 'monto';
    const expectedDescription = descriptions['indicator_5'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_5_6 when toggleSelectedValue is "fecha"', () => {
    const indicator = 'indicator_5_6';
    const toggleSelectedValue = 'fecha';
    const expectedDescription = descriptions['indicator_6'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_7_8 when toggleSelectedValue is "monto"', () => {
    const indicator = 'indicator_7_8';
    const toggleSelectedValue = 'monto';
    const expectedDescription = descriptions['indicator_7'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_7_8 when toggleSelectedValue is "fecha"', () => {
    const indicator = 'indicator_7_8';
    const toggleSelectedValue = 'fecha';
    const expectedDescription = descriptions['indicator_8'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_9', () => {
    const indicator = 'indicator_9';
    const toggleSelectedValue = '';

    const expectedDescription = descriptions['indicator_9'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_10_11 when toggleSelectedValue is "licitacion"', () => {
    const indicator = 'indicator_10_11';
    const toggleSelectedValue = 'licitacion';
    const expectedDescription = descriptions['indicator_10'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_10_11 when toggleSelectedValue is "adjudicacion"', () => {
    const indicator = 'indicator_10_11';
    const toggleSelectedValue = 'adjudicacion';
    const expectedDescription = descriptions['indicator_11'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_12', () => {
    const indicator = 'indicator_12';
    const toggleSelectedValue = '';

    const expectedDescription = descriptions['indicator_12'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_13', () => {
    const indicator = 'indicator_13';
    const toggleSelectedValue = '';

    const expectedDescription = descriptions['indicator_13'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_14', () => {
    const indicator = 'indicator_14';
    const toggleSelectedValue = '';

    const expectedDescription = descriptions['indicator_14'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_15', () => {
    const indicator = 'indicator_15';
    const toggleSelectedValue = '';

    const expectedDescription = descriptions['indicator_15'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_16', () => {
    const indicator = 'indicator_16';
    const toggleSelectedValue = '';

    const expectedDescription = descriptions['indicator_16'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns the correct description for indicator_17', () => {
    const indicator = 'indicator_17';
    const toggleSelectedValue = '';

    const expectedDescription = descriptions['indicator_17'];

    const result = getIndicatorDescription(indicator, toggleSelectedValue);

    expect(result).toEqual(expectedDescription);
  });

  it('returns undefined for an unknown indicator', () => {

    const toggleSelectedValue = '';

    expect(getIndicatorDescription('indicator_unknown', toggleSelectedValue))
      .toBe(undefined);
  });
});