import { shallowMount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';
import DynamicHeaderComponent from 'src/components/indicator-component/dynamic-header-component/DynamicHeaderComponent.vue';

describe('DynamicHeaderComponent', () => {
  it('renders dynamic header items', () => {
    const dynamicHeader = [
      { title: 'Title 1', value: 'Value 1' },
      { title: 'Title 2', value: 'Value 2' },
    ];
    const wrapper = shallowMount(DynamicHeaderComponent, {
      props: { dynamicHeader },
    });

    const titleElements = wrapper.findAll('.dynamic_header__title');
    const valueElements = wrapper.findAll('.dynamic_header__value');

    expect(titleElements).toHaveLength(dynamicHeader.length);
    expect(valueElements).toHaveLength(dynamicHeader.length);
    expect(titleElements[0].text()).toMatch(dynamicHeader[0].title);
    expect(valueElements[0].text()).toMatch(dynamicHeader[0].value);
    expect(titleElements[1].text()).toMatch(dynamicHeader[1].title);
    expect(valueElements[1].text()).toMatch(dynamicHeader[1].value);
  });
});
