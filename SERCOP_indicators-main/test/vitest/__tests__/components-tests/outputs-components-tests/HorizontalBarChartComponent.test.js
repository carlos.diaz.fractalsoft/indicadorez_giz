import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';
import HorizontalBarChartComponent from 'src/components/indicator-component/outputs-component/horizontal-bar-chart-component/HorizontalBarChartComponent.vue';

describe('HorizontalBarChartComponent', () => {
  it('renders chart with correct data', () => {
    const propsData = {
      data: [
        { entidad_contratante: 'Entity 1', register_number: 50 },
        { entidad_contratante: 'Entity 2', register_number: 20 },
        { entidad_contratante: 'Entity 3', register_number: 80 },
      ],
      indicator: 'indicator_5_6'
    };
    const wrapper = mount(HorizontalBarChartComponent, { propsData });

    // Assert chart x-axis label
    expect(wrapper.find('.hor-bar-chart__title-row__register-number').text()).toMatch('Número de registros incompletos');

    // Assert chart data
    const rows = wrapper.findAll('.hor-bar-chart__content-part__single-organization-row');
    expect(rows).toHaveLength(3);
    expect(rows[0].find('.hor-bar-chart__content-part__single-organization-row__name-with-bar__name').text()).toMatch('Entity 3');
    expect(rows[1].find('.hor-bar-chart__content-part__single-organization-row__name-with-bar__name').text()).toMatch('Entity 1');
    expect(rows[2].find('.hor-bar-chart__content-part__single-organization-row__name-with-bar__name').text()).toMatch('Entity 2');
  })
})