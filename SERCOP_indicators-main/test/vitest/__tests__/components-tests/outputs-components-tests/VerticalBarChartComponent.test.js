import { mount } from '@vue/test-utils';
import { describe, expect, it } from 'vitest';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';
import VerticalBarChartComponent from 'src/components/indicator-component/outputs-component/vertical-bar-chart-component/VerticalBarChartComponent.vue'

installQuasarPlugin();

describe('VerticalBarChartComponent', () => {
  const data = [    { yearmonth: '2022.01', porcentaje: 60 },    { yearmonth: '2022.02', porcentaje: 80 },    { yearmonth: '2022.03', porcentaje: 50 }  ]
  const props = {
    data,
    indicator: 'indicator_1_2'
  };

  it('renders the y-axis label', () => {
    const wrapper = mount(VerticalBarChartComponent, { props });
    expect(wrapper.find('.y-axis-label span').text()).toBe('Porcentaje (%) de registros con información incompleta');
  });

  it('renders the x-axis label', () => {
    const wrapper = mount(VerticalBarChartComponent, { props });
    expect(wrapper.find('.x-axis-label span').text()).toBe('Meses');
  });

  it('renders the correct number of scale steps', () => {
    const wrapper = mount(VerticalBarChartComponent, { props });
    expect(wrapper.findAll('.ver-bar-chart__scale-area__scale-step')).toHaveLength(11);
  });

  it('renders the correct number of content columns', () => {
    const wrapper = mount(VerticalBarChartComponent, { props });
    expect(wrapper.findAll('.ver-bar-chart__content-column')).toHaveLength(3);
  });

  it('renders the content columns with the correct values in correct order', () => {
    const wrapper = mount(VerticalBarChartComponent, { props });
    const contentColumns = wrapper.findAll('.ver-bar-chart__content-column');
    expect(contentColumns[0].find('.ver-bar-chart__content-column__header-with-value').text()).toBe('60 %');
    expect(contentColumns[0].find('.ver-bar-chart__content-column__footer-with-date').text()).toBe('2022.01');
    expect(contentColumns[1].find('.ver-bar-chart__content-column__header-with-value').text()).toBe('80 %');
    expect(contentColumns[1].find('.ver-bar-chart__content-column__footer-with-date').text()).toBe('2022.02');
    expect(contentColumns[2].find('.ver-bar-chart__content-column__header-with-value').text()).toBe('50 %');
    expect(contentColumns[2].find('.ver-bar-chart__content-column__footer-with-date').text()).toBe('2022.03');
  });
});