import { describe, expect, it } from 'vitest';
import { mount, shallowMount } from '@vue/test-utils';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';

import MultipleChoiceSelectorComponent from 'src/components/indicator-component/inputs-component/multiple-choice-component/MultipleChoiceSelectorComponent.vue';
import getIndicatorModel from 'src/composables/get_indicator_model';

installQuasarPlugin();

describe('MultipleChoiceSelectorComponent', () => {

  const options = [
    { label: 'Azuay', value: 'AZUAY', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Bolívar', value: 'BOLIVAR', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Cañar', value: 'CAÑAR', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Carchi', value: 'CARCHI', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Chimborazo', value: 'CHIMBORAZO', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Cotopaxi', value: 'COTOPAXI', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'El Oro', value: 'EL ORO', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Esmeraldas', value: 'ESMERALDAS', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Galápagos', value: 'GALAPAGOS', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Guayas', value: 'GUAYAS', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Imbabura', value: 'IMBABURA', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Loja', value: 'LOJA', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Los Ríos', value: 'LOS RIOS', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Manabí', value: 'MANABI', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Morona Santiago', value: 'MORONA SANTIAGO', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Napo', value: 'NAPO', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Orellana', value: 'ORELLANA', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Pastaza', value: 'PASTAZA', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Pichincha', value: 'PICHINCHA', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Santa Elena', value: 'SANTA ELENA', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Santo Domingo de los Tsáchilas', value: 'SANTO DOMINGO DE LOS TSACHILAS', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Sucumbíos', value: 'SUCUMBIOS', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Tungurahua', value: 'TUNGURAHUA', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
    { label: 'Zamora Chinchipe', value: 'ZAMORA CHINCHIPE', checkedIcon: 'radio_button_checked', uncheckedIcon: 'radio_button_unchecked' },
  ];

  const testData = [
    {
      indicatorType: 'indicator_1_2',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_3_4',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_5_6',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_7_8',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_9',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_10_11',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_12',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_13',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_14',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
    {
      indicatorType: 'indicator_15',
      expectedLabel: 'Provincia',
      expectedOptions: options,
    },
  ];

  testData.forEach(({ indicatorType, expectedLabel, expectedOptions }) => {
    const indicatorDataModel = getIndicatorModel(indicatorType);

    it('renders without crashing with correct label', () => {
      const wrapper = shallowMount(MultipleChoiceSelectorComponent, {
        propsData: {
          indicatorType,
          modelValue: indicatorDataModel.provinciasSelectedValues,
          isFetching: false,
        },
      });
      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('p.province-selector__selector-label').text()).toBe(expectedLabel);
    });

    it(`displays the correct options for ${indicatorType}`, () => {
      const wrapper = shallowMount(MultipleChoiceSelectorComponent, {
        propsData: {
          indicatorType,
          modelValue: indicatorDataModel.provinciasSelectedValues,
          isFetching: false,
        },
      });
      expect(wrapper.vm.options).toEqual(expectedOptions);
    });
  });
});