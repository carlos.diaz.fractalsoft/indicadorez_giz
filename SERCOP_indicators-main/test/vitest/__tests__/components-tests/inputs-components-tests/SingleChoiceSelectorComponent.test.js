import { describe, expect, it } from 'vitest';
import { mount, shallowMount } from '@vue/test-utils';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';

import SingleChoiceSelectorComponent from 'src/components/indicator-component/inputs-component/single-choice-selector-component/SingleChoiceSelectorComponent.vue';
import getIndicatorModel from 'src/composables/get_indicator_model';

installQuasarPlugin();

const contractingProceduresOptions = [
  {label: 'Asesoría y Patrocinio Jurídico', value: 'asesoria_y_patrocinio_juridico'},
  {label: 'Asesoría y Patrocinio Jurídico – Cons. puntuales y específicas', value: 'asesoria_y_patrocinio_juridico_cons_puntuales_y_especificas'},
  {label: 'Bienes y Servicios únicos', value: 'bienes_y_servicios_unicos'},
  {label: 'Comunicación Social – Contratación Directa', value: 'comunicacion_social_contratacion_directa'},
  {label: 'Comunicación Social – Proceso de Selección', value: 'comunicacion_social_proceso_de_seleccion'},
  {label: 'Concurso público', value: 'concurso_publico'},
  {label: 'Concurso Público por Contratación Directa Desierta', value: 'concurso_publico_por_contratacion_desierta'},
  {label: 'Concurso Público por Lista Corta Desierta', value: 'concurso_publico_por_lista_corta_desierta'},
  {label: 'Cont. de Instituciones Financieras y de Seguros del Estado', value: 'cont_de_instituciones_financieras_y_de_seguros_del_estado'},
  {label: 'Contratación de Seguros', value: 'contratacion_de_seguros'},
  {label: 'Contratación directa', value: 'contratacion_directa'},
  {label: 'Contratación Directa por Terminación Unilateral', value: 'contratacion_directa_por_terminacion_unilateral'},
  {label: 'Contrataciones con empresas públicas internacionales', value: 'contrataciones_con_empresas_publicas_internacionales'},
  {label: 'Contratos entre Entidades Públicas o sus subsidiarias', value: 'contratos_entre_entidades_publicas_o_sus_subsidiarias'},
  {label: 'Convenio Marco', value: 'convenio_marco'},
  {label: 'Cotización', value: 'cotizacion'},
  {label: 'Empresas Públicas, Mercantiles o Subsidiarias', value: 'empresas_publicas_mercantiles_o_subsidiarias'},
  {label: 'Licitación', value: 'licitacion'},
  {label: 'Licitación de Seguros', value: 'licitacion_de_seguros'},
  {label: 'Lista corta', value: 'lista_corta'},
  {label: 'Lista Corta por Contratación Directa Desierta', value: 'lista_corta_por_contratacion_directa_desierta'},
  {label: 'Menor Cuantía', value: 'menor_cuantia'},
  {label: 'Obra artística, científica o literaria', value: 'obra_artistica_cientifica_o_literaria'},
  {label: 'Repuestos o Accesorios', value: 'repuestos_o_accesorios'},
  {label: 'Sectores Estratégicos', value: 'sectores_estrategicos'},
  {label: 'Subasta Inversa Electrónica', value: 'subasta_inversa_electronica'},
  {label: 'Transporte de correo interno o internacional', value: 'transporte_de_correo_interno_o_internacional'},
];

const processFasesOptions = [
  { label: 'Planning (planificación)', value: 'planificacion' },
  { label: 'Tender (licitación)', value: 'licitacion' },
  { label: 'Awards (adjudicación)', value: 'adjudicacion' },
  { label: 'Contracts (contractual)', value: 'contractual' },
];

describe('SingleChoiceSelectorComponent', () => {

  const testData = [
    {
      indicatorType: 'indicator_1_2',
      expectedLabel: 'Fase del proceso',
      expectedOptions: processFasesOptions,
    },
    {
      indicatorType: 'indicator_3_4',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
    {
      indicatorType: 'indicator_7_8',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
    {
      indicatorType: 'indicator_9',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
    {
      indicatorType: 'indicator_10_11',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
    {
      indicatorType: 'indicator_14',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
    {
      indicatorType: 'indicator_15',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
    {
      indicatorType: 'indicator_16',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
    {
      indicatorType: 'indicator_17',
      expectedLabel: 'Procedimiento de contratación',
      expectedOptions: contractingProceduresOptions,
    },
  ];

  testData.forEach(({ indicatorType, expectedLabel, expectedOptions }) => {
    const indicatorDataModel = getIndicatorModel(indicatorType);

    it('renders without crashing with correct label', () => {
      const wrapper = mount(SingleChoiceSelectorComponent, {
        propsData: {
          indicatorType,
          modelValue: indicatorDataModel.singleChoiseSelectedValue,
          isFetching: false,
        },
      });
      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('p.single-choice-selector__selector-label').text()).toBe(expectedLabel);
    });

    it(`displays the correct options for ${indicatorType}`, () => {
      const wrapper = shallowMount(SingleChoiceSelectorComponent, {
        propsData: {
          indicatorType,
          modelValue: indicatorDataModel.singleChoiseSelectedValue,
          isFetching: false,
        },
      });
      expect(wrapper.vm.options).toEqual(expectedOptions);
    });
  });
});
