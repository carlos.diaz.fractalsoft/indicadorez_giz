import { describe, expect, it } from 'vitest';
import { shallowMount } from '@vue/test-utils';

import InputsComponent from 'src/components/indicator-component/inputs-component/InputsComponent.vue';
import getIndicatorModel from 'src/composables/get_indicator_model';

function getExpectedComponents(indicator) {
  const isRadioToggleComponent = ['indicator_1_2', 'indicator_3_4', 'indicator_5_6', 'indicator_7_8', 'indicator_10_11'].includes(indicator);
  const isSingleChoiceSelectorComponent = !['indicator_5_6', 'indicator_12', 'indicator_13'].includes(indicator);
  const isMultipleChoiceSelectorComponent = !['indicator_16', 'indicator_17'].includes(indicator);

  return {
    isRadioToggleComponent,
    isSingleChoiceSelectorComponent,
    isMultipleChoiceSelectorComponent,
  };
};

describe('InputsComponent', () => {

  it('renders without crashing', () => {
    const indicatorDataModel = getIndicatorModel('indicator_1_2');
    const wrapper = shallowMount(InputsComponent, {
      propsData: {
        indicator: 'indicator_1_2',
        modelValue: indicatorDataModel,
        isFetching: false,
      },
    });
    expect(wrapper.exists()).toBe(true);
  });

  it.each([
    [{ start: { year: 2023, month: 2 }, end: { year: 2023, month: 4 } }, true],
    [{ start: { year: 2023, month: 2 }, end: { year: 2023, month: 2 } }, true],
    [{ start: { year: 2022, month: 2 }, end: { year: 2021, month: 0 } }, false],
  ])('should set isDateRangeValid correctly', (dateRange, expected) => {
    const indicatorDataModel = getIndicatorModel('indicator_1_2');

    indicatorDataModel.userDateRangeQuery.userDateQueryStart.pickedYear = dateRange.start.year;
    indicatorDataModel.userDateRangeQuery.userDateQueryStart.pickedMonth = dateRange.start.month;
    indicatorDataModel.userDateRangeQuery.userDateQueryEnd.pickedYear = dateRange.end.year;
    indicatorDataModel.userDateRangeQuery.userDateQueryEnd.pickedMonth = dateRange.end.month;

    const wrapper = shallowMount(InputsComponent, {
      propsData: {
        indicator: 'indicator_1_2',
        modelValue: indicatorDataModel,
        isFetching: false,
      },
    });

    wrapper.vm.checkDateRangeValid();
    expect(wrapper.vm.isDateRangeValid).toBe(expected);
  });

  it('renders the correct inputs for each indicator prop value', () => {
    const indicators = [
      'indicator_1_2',
      'indicator_3_4',
      'indicator_5_6',
      'indicator_7_8',
      'indicator_9',
      'indicator_10_11',
      'indicator_12',
      'indicator_13',
      'indicator_14',
      'indicator_15',
      'indicator_16',
      'indicator_17',
    ];

    indicators.forEach((indicator) => {
      const indicatorDataModel = getIndicatorModel(indicator);

      const wrapper = shallowMount(InputsComponent, {
        propsData: {
          indicator,
          modelValue: indicatorDataModel,
          isFetching: false,
        },
      });

      const expectedComponents = getExpectedComponents(indicator);

      expect(wrapper.findComponent({ name: 'RadioToggleComponent' }).exists())
        .toBe(expectedComponents.isRadioToggleComponent);
      expect(wrapper.findComponent({ name: 'SingleChoiceSelectorComponent' }).exists())
        .toBe(expectedComponents.isSingleChoiceSelectorComponent);
      expect(wrapper.findComponent({ name: 'MultipleChoiceSelectorComponent' }).exists())
        .toBe(expectedComponents.isMultipleChoiceSelectorComponent);
      expect(wrapper.findAllComponents({ name: 'DatePickerComponent' }).length).toBe(2);
    });
  });
});
