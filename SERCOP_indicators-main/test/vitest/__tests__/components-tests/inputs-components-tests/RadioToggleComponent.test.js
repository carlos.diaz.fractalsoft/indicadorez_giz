import { describe, expect, it } from 'vitest';
import { mount, shallowMount } from '@vue/test-utils';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';

import RadioToggleComponent from 'src/components/indicator-component/inputs-component/radio-toggle-component/RadioToggleComponent.vue';
import getIndicatorModel from 'src/composables/get_indicator_model';

installQuasarPlugin();

const paramsInformacionIncompletaOptions = [
  { label: 'Monto', value: 'monto' },
  { label: 'Fecha', value: 'fecha' },
];

const faseProcedimientoContratacionOptions = [
  { label: 'Tender (licitación)', value: 'licitacion' },
  { label: 'Awards (adjudicación)', value: 'adjudicacion' },
];

describe('RadioToggleComponent', () => {

  const testData = [
    {
      indicatorType: 'indicator_1_2',
      expectedOptions: paramsInformacionIncompletaOptions,
    },
    {
      indicatorType: 'indicator_3_4',
      expectedOptions: paramsInformacionIncompletaOptions,
    },
    {
      indicatorType: 'indicator_5_6',
      expectedOptions: paramsInformacionIncompletaOptions,
    },
    {
      indicatorType: 'indicator_7_8',
      expectedOptions: paramsInformacionIncompletaOptions,
    },
    {
      indicatorType: 'indicator_10_11',
      expectedOptions: faseProcedimientoContratacionOptions,
    },
  ];

  testData.forEach(({ indicatorType, expectedOptions }) => {

    const indicatorDataModel = getIndicatorModel(indicatorType);

    it('renders without crashing with correct label', () => {
      const wrapper = shallowMount(RadioToggleComponent, {
        propsData: {
          indicatorType,
          modelValue: indicatorDataModel.toggleSelectedValue,
          isFetching: false,
        },
      })
      expect(wrapper.exists()).toBe(true);
      expect(wrapper.find('.radio_buttons_group__label p').text()).toBe('Parámetros información incompleta');
    });

    it(`displays the correct options for ${indicatorType}`, () => {
      const wrapper = shallowMount(RadioToggleComponent, {
        propsData: {
          indicatorType,
          modelValue: indicatorDataModel.toggleSelectedValue,
          isFetching: false,
        },
      });
      expect(wrapper.vm.options).toEqual(expectedOptions);
    });

    it('emits the update:modelValue event when an option is selected', () => {
      const wrapper = mount(RadioToggleComponent, {
        props: {
          indicatorType,
          modelValue: indicatorDataModel.toggleSelectedValue,
          isFetching: false,
        },
      })
      const option = wrapper.find('.q-radio[aria-checked="false"]');
      option.trigger('click');
      expect(wrapper.emitted('update:modelValue')).toBeTruthy();
    });
  });
});