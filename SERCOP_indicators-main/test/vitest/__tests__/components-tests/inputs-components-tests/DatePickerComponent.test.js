import { describe, expect, it } from 'vitest';
import { mount, shallowMount } from '@vue/test-utils';
import { installQuasarPlugin } from '@quasar/quasar-app-extension-testing-unit-vitest';

import DatePickerComponent from 'src/components/indicator-component/inputs-component/date-picker-component/DatePickerComponent.vue';
import getIndicatorModel from 'src/composables/get_indicator_model';

installQuasarPlugin();

describe('DatePickerComponent', () => {
  it('renders the component', () => {
    const wrapper = mount(DatePickerComponent, {
      props: {
        startPossibleYearOfPeriod: 2000,
        isDateRangeValid: true,
        selectorLabel: 'Fecha inicio',
        modelValue: { pickedYear: 2022, pickedMonth: 2 },
        isFetching: false,
      },
    });

    expect(wrapper.exists()).toBe(true);
  });
});
