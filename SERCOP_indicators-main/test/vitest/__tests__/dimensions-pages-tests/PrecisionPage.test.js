import { describe, expect, test } from 'vitest';
import { shallowMount } from '@vue/test-utils';

import PrecisionPage from 'src/pages/PrecisionPage.vue';
import IndicatorComponent from 'src/components/indicator-component/IndicatorComponent.vue';

describe('PrecisionPage', () => {
  test('renders IndicatorComponent for each indicator', () => {
    const wrapper = shallowMount(PrecisionPage);

    // Asserts that the component renders 3 indicator components
    expect(wrapper.findAllComponents(IndicatorComponent)).toHaveLength(3);

    // Asserts that each IndicatorComponent receives the correct prop
    const expectedIndicators = ['indicator_15', 'indicator_16', 'indicator_17'];
    wrapper.findAllComponents(IndicatorComponent).forEach((indicatorWrapper, index) => {
      expect(indicatorWrapper.props('indicatorType')).toBe(expectedIndicators[index]);
    });
  });
});