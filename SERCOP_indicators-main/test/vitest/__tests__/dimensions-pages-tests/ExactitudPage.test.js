import { describe, expect, test } from 'vitest';
import { shallowMount } from '@vue/test-utils';

import ExactitudPage from 'src/pages/ExactitudPage.vue';
import IndicatorComponent from 'src/components/indicator-component/IndicatorComponent.vue';

describe('ExactitudPage', () => {
  test('renders IndicatorComponent for each indicator', () => {
    const wrapper = shallowMount(ExactitudPage);

    // Asserts that the component renders 4 indicator components
    expect(wrapper.findAllComponents(IndicatorComponent)).toHaveLength(4);

    // Asserts that each IndicatorComponent receives the correct prop
    const expectedIndicators = ['indicator_10_11', 'indicator_12', 'indicator_13', 'indicator_14'];
    wrapper.findAllComponents(IndicatorComponent).forEach((indicatorWrapper, index) => {
      expect(indicatorWrapper.props('indicatorType')).toBe(expectedIndicators[index]);
    });
  });
});