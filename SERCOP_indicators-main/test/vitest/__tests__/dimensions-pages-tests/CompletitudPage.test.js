import { describe, expect, test } from 'vitest';
import { shallowMount } from '@vue/test-utils';

import CompletitudPage from 'src/pages/CompletitudPage.vue';
import IndicatorComponent from 'src/components/indicator-component/IndicatorComponent.vue';

describe('CompletitudPage', () => {
  test('renders IndicatorComponent for each indicator', () => {
    const wrapper = shallowMount(CompletitudPage);

    // Asserts that the component renders 4 indicator components
    expect(wrapper.findAllComponents(IndicatorComponent)).toHaveLength(4);

    // Asserts that each IndicatorComponent receives the correct prop
    const expectedIndicators = ['indicator_1_2', 'indicator_3_4', 'indicator_5_6', 'indicator_7_8'];
    wrapper.findAllComponents(IndicatorComponent).forEach((indicatorWrapper, index) => {
      expect(indicatorWrapper.props('indicatorType')).toBe(expectedIndicators[index]);
    });
  });
});
