import { describe, expect, test } from 'vitest';
import { shallowMount } from '@vue/test-utils';

import OportunidadPage from 'src/pages/OportunidadPage.vue';
import IndicatorComponent from 'src/components/indicator-component/IndicatorComponent.vue';

describe('OportunidadPage', () => {
  test('renders IndicatorComponent for each indicator', () => {
    const wrapper = shallowMount(OportunidadPage);

    // Asserts that the component renders 1 indicator component
    expect(wrapper.findAllComponents(IndicatorComponent)).toHaveLength(1);

    // Asserts that each IndicatorComponent receives the correct prop
    const expectedIndicators = ['indicator_9'];
    wrapper.findAllComponents(IndicatorComponent).forEach((indicatorWrapper, index) => {
      expect(indicatorWrapper.props('indicatorType')).toBe(expectedIndicators[index]);
    });
  });
});