const urlBasePart = import.meta.env.VITE_VUE_DOWNLOAD_BASE_URL;

const methodologiesLinksList = {
  indicator_1:    `${urlBasePart}1.pdf`,
  indicator_2:    `${urlBasePart}2.pdf`,
  indicator_3:    `${urlBasePart}3.pdf`,
  indicator_4:    `${urlBasePart}4.pdf`,
  indicator_5:    `${urlBasePart}5.pdf`,
  indicator_6:    `${urlBasePart}6.pdf`,
  indicator_7:    `${urlBasePart}7.pdf`,
  indicator_8:    `${urlBasePart}8.pdf`,
  indicator_9:    `${urlBasePart}9.pdf`,
  indicator_10:   `${urlBasePart}10.pdf`,
  indicator_11:   `${urlBasePart}11.pdf`,
  indicator_12:   `${urlBasePart}12.pdf`,
  indicator_13:   `${urlBasePart}13.pdf`,
  indicator_14:   `${urlBasePart}14.pdf`,
  indicator_15:   `${urlBasePart}15.pdf`,
  indicator_16:   `${urlBasePart}16.pdf`,
  indicator_17:   `${urlBasePart}17.pdf`,
};

const getMethodologyDownloadLink = (indicator, toggleSelectedValue) => {
  let metodologyDownloadLink = '';

  switch (indicator) {
    case 'indicator_1_2':
      metodologyDownloadLink = toggleSelectedValue === 'monto' ? methodologiesLinksList['indicator_1'] : methodologiesLinksList['indicator_2'];
      break;

    case 'indicator_3_4':
      metodologyDownloadLink = toggleSelectedValue === 'monto' ? methodologiesLinksList['indicator_3'] : methodologiesLinksList['indicator_4'];
      break;

    case 'indicator_5_6':
      metodologyDownloadLink = toggleSelectedValue === 'monto' ? methodologiesLinksList['indicator_5'] : methodologiesLinksList['indicator_6'];
      break;

    case 'indicator_7_8':
      metodologyDownloadLink = toggleSelectedValue === 'monto' ? methodologiesLinksList['indicator_7'] : methodologiesLinksList['indicator_8'];
      break;

    case 'indicator_10_11':
      metodologyDownloadLink = toggleSelectedValue === 'licitacion' ? methodologiesLinksList['indicator_10'] : methodologiesLinksList['indicator_11'];
      break;

    case 'indicator_9':
    case 'indicator_12':
    case 'indicator_13':
    case 'indicator_14':
    case 'indicator_15':
    case 'indicator_16':
    case 'indicator_17':
      metodologyDownloadLink = methodologiesLinksList[indicator];
      break;
  }
  return metodologyDownloadLink;
};

export default getMethodologyDownloadLink;
