import getNormalizedName from "./get_normalized_name";

const getFormattedDate = function(year, month) {
  const formattedMonth = `${parseInt(month) + 1}`.padStart(2, '0');
  return `${year}-${formattedMonth}-01`;
};

const fetchData = async (indicatorType, dataModel) => {

  const urlBasePart = import.meta.env.VITE_VUE_APP_BASE_URL;

  let indicator = '';
  let url = '';
  let minDate = getFormattedDate(dataModel.userDateRangeQuery.userDateQueryStart.pickedYear, dataModel.userDateRangeQuery.userDateQueryStart.pickedMonth);
  let maxDate = getFormattedDate(dataModel.userDateRangeQuery.userDateQueryEnd.pickedYear, dataModel.userDateRangeQuery.userDateQueryEnd.pickedMonth);
  let params;

  switch (indicatorType) {

    case 'indicator_1_2':
      indicator = dataModel.toggleSelectedValue === 'monto' ? 'indicador1' : 'indicador2';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
        `proceso=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'process_fase')}`
      ];
      break;

    case 'indicator_3_4':
      indicator = dataModel.toggleSelectedValue === 'monto' ? 'indicador3' : 'indicador4';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;

    case 'indicator_5_6':
      indicator = dataModel.toggleSelectedValue === 'monto' ? 'indicador5' : 'indicador6';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
      ];
      break;

    case 'indicator_7_8':
      indicator = dataModel.toggleSelectedValue === 'monto' ? 'indicador7' : 'indicador8';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;

    case 'indicator_9':
      indicator = 'indicador9';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;

    case 'indicator_10_11':
      indicator = dataModel.toggleSelectedValue === 'licitacion' ? 'indicador10' : 'indicador11';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;

    case 'indicator_12':
      indicator = 'indicador12';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
      ];
      break;

    case 'indicator_13':
      indicator = 'indicador13';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
      ];
      break;

    case 'indicator_14':
      indicator = 'indicador14';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;

    case 'indicator_15':
      indicator = 'indicador15';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `region=[${dataModel.provinciasSelectedValues.join(',')}]`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;

    case 'indicator_16':
      indicator = 'indicador16';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;

    case 'indicator_17':
      indicator = 'indicador17';
      params = [
        `mindate=${minDate}`,
        `maxdate=${maxDate}`,
        `procedimiento=${getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure')}`
      ];
      break;
  }

  url = `${urlBasePart}/${indicator}?` + params.join('&');

  try {
    const response = await fetch(url, {
      method: 'GET',
      cache: 'no-store',
    });
    if (response.ok) {
      const jsonData = await response.json();
      return jsonData;
    }
  } catch (error) {
    console.error(error);
  }
};

export default fetchData;