import getNormalizedName from "./get_normalized_name";

const makeDateRangeQueryString = function(dateRangeQueryObject) {
  return `${getNormalizedName(dateRangeQueryObject.userDateQueryStart.pickedMonth.toString(), 'month_full_name')} ${dateRangeQueryObject.userDateQueryStart.pickedYear} - ${getNormalizedName(dateRangeQueryObject.userDateQueryEnd.pickedMonth.toString(), 'month_full_name')} ${dateRangeQueryObject.userDateQueryEnd.pickedYear}`;
};

const makeSelectedProvinciasString = function(selectedProvinciasObject) {
  return selectedProvinciasObject.length === 24 ? 'Todas' : [...selectedProvinciasObject].map(elem => getNormalizedName(elem, 'region')).join(", ");
};

const getDynamicHeader = function(dataModel, indicator) {
  let dynamicHeader = [];

  switch (indicator) {

      case 'indicator_1_2':
        dynamicHeader = [
          {
            "title":'Parámetro de información incompleta: ',
            "value": dataModel.toggleSelectedValue[0].toUpperCase() + dataModel.toggleSelectedValue.slice(1),
          },
          {
            "title":'Provincias seleccionadas: ',
            "value": makeSelectedProvinciasString(dataModel.provinciasSelectedValues),
          },
          {
            "title":'Fase de proceso seleccionada: ',
            "value": getNormalizedName(dataModel.singleChoiseSelectedValue, 'process_fase'),
          },
          {
            "title":'Fechas seleccionadas: ',
            "value": makeDateRangeQueryString(dataModel.userDateRangeQuery),
          }
        ]
        break;

      case 'indicator_3_4':
      case 'indicator_7_8':
        dynamicHeader = [
          {
            "title":'Parámetro de información incompleta: ',
            "value": dataModel.toggleSelectedValue[0].toUpperCase() + dataModel.toggleSelectedValue.slice(1),
          },
          {
            "title":'Provincias seleccionadas: ',
            "value": makeSelectedProvinciasString(dataModel.provinciasSelectedValues),
          },
          {
            "title":'Procedimiento de contratación: ',
            "value": getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure'),
          },
          {
            "title":'Fechas seleccionadas: ',
            "value": makeDateRangeQueryString(dataModel.userDateRangeQuery),
          }
        ]
        break;

      case 'indicator_5_6':
        dynamicHeader = [
          {
            "title":'Parámetro de información incompleta: ',
            "value": dataModel.toggleSelectedValue[0].toUpperCase() + dataModel.toggleSelectedValue.slice(1),
          },
          {
            "title":'Provincias seleccionadas: ',
            "value": makeSelectedProvinciasString(dataModel.provinciasSelectedValues),
          },
          {
            "title":'Fechas seleccionadas: ',
            "value": makeDateRangeQueryString(dataModel.userDateRangeQuery),
          }
        ]
        break;

      case 'indicator_9':
      case 'indicator_14':
      case 'indicator_15':
        dynamicHeader = [
          {
            "title":'Provincias seleccionadas: ',
            "value": makeSelectedProvinciasString(dataModel.provinciasSelectedValues),
          },
          {
            "title":'Procedimiento de contratación: ',
            "value": getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure'),
          },
          {
            "title":'Fechas seleccionadas: ',
            "value": makeDateRangeQueryString(dataModel.userDateRangeQuery),
          }
        ]
        break;

      case 'indicator_10_11':
        dynamicHeader = [
          {
            "title":'Parámetro de información incompleta: ',
            "value": getNormalizedName(dataModel.toggleSelectedValue, 'contracting_procedures_fase'),
          },
          {
            "title":'Provincias seleccionadas: ',
            "value": makeSelectedProvinciasString(dataModel.provinciasSelectedValues),
          },
          {
            "title":'Procedimiento de contratación: ',
            "value": getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure'),
          },
          {
            "title":'Fechas seleccionadas: ',
            "value": makeDateRangeQueryString(dataModel.userDateRangeQuery),
          }
        ]
        break;

      case 'indicator_12':
      case 'indicator_13':
        dynamicHeader = [
          {
            "title":'Provincias seleccionadas: ',
            "value": makeSelectedProvinciasString(dataModel.provinciasSelectedValues),
          },
          {
            "title":'Fechas seleccionadas: ',
            "value": makeDateRangeQueryString(dataModel.userDateRangeQuery),
          }
        ]
        break;

      case 'indicator_16':
      case 'indicator_17':
        dynamicHeader = [
          {
            "title":'Procedimiento de contratación: ',
            "value": getNormalizedName(dataModel.singleChoiseSelectedValue, 'contracting_procedure'),
          },
          {
            "title":'Fechas seleccionadas: ',
            "value": makeDateRangeQueryString(dataModel.userDateRangeQuery),
          }
        ]
        break;
  }

  return dynamicHeader;
};

export default getDynamicHeader;
