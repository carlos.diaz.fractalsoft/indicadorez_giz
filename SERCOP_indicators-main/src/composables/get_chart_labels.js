const chartLabels = {
  'indicator_1_2':    { xAxisLabel: 'Meses', yAxisLabel: 'Porcentaje (%) de registros con información incompleta' },
  'indicator_3_4':    { xAxisLabel: 'Meses', yAxisLabel: 'Porcentaje (%) de registros con información incompleta' },
  'indicator_5_6':    { xAxisLabel: 'Número de registros incompletos', yAxisLabel: 'Enti&shy;dades contra&shy;tantes' },
  'indicator_7_8':    { xAxisLabel: 'Número de registros incompletos', yAxisLabel: 'Enti&shy;dades contra&shy;tantes' },
  'indicator_9':      { xAxisLabel: 'Meses', yAxisLabel: 'Porcentaje de registros donde la diferencia entre la fecha de adjudicación y la fecha actual es mayor a 30 días' },
  'indicator_10_11':  { xAxisLabel: 'Meses', yAxisLabel: 'Número de registros de contratación pública con montos de adjudicación con valores cercanos a $0'},
  'indicator_12':     { xAxisLabel: 'Número de registros con monto < $0.5', yAxisLabel: 'Proveedor'},
  'indicator_13':     { xAxisLabel: 'Número de registros con monto < $0.5', yAxisLabel: 'Entidades contratantes'},
  'indicator_14':     { xAxisLabel: 'Meses', yAxisLabel: 'Número de registros donde el monto adjudicado ha sido corregido por el SERCOP'},
  'indicator_15':     { xAxisLabel: 'Meses', yAxisLabel: 'Variabilidad en monto adjudicado corregido por el SERCOP'},
  'indicator_16':     { xAxisLabel: 'Meses', yAxisLabel: 'Monto Licitado'},
  'indicator_17':     { xAxisLabel: 'Meses', yAxisLabel: 'Monto Adjudicado'},
};

const getChartLabels = (indicator) => chartLabels[indicator];

export default getChartLabels;
