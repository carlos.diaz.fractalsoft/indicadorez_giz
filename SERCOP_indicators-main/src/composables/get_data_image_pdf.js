import { render, h, nextTick } from 'vue';

import domToImage from 'dom-to-image';
import { jsPDF } from "jspdf";

import ReportComponent from 'src/components/report-component/ReportComponent.vue';

const makeReport = async (
  indicator,
  indicatorTitle,
  indicatorDescription,
  dynamicHeader,
  data,
  extension) => {

  const reportProps = {
    indicator: indicator,
    indicatorTitle: indicatorTitle,
    indicatorDescription: indicatorDescription,
    dynamicHeader: dynamicHeader,
    data: data,
  };

  const reportComponent = h(ReportComponent, reportProps);
  const tempContainer = document.createElement('div');

  document.body.appendChild(tempContainer);
  render(reportComponent, tempContainer);

  await nextTick();

  let report;

  if (extension === 'png' || extension === 'jpg') {

    if (extension === 'png' ) {
      report = await domToImage.toPng(tempContainer.querySelector('.report'), { bgcolor: '#FFFFFF' });
    } else if (extension === 'jpg') {
      report = await domToImage.toJpeg(tempContainer.querySelector('.report'), { bgcolor: '#FFFFFF' });
    }

    document.body.removeChild(tempContainer);
    const link = document.createElement('a');
    link.href = report;
    link.download = `report.${extension}`;
    link.click();

  } else if (extension === 'pdf') {

    report = await domToImage.toPng(tempContainer.querySelector('.report'));

    let width;
    let height;
    const img = new Image();
    img.src = report;

    const orientation = ['indicator_5_6', 'indicator_7_8', 'indicator_12', 'indicator_13'].includes(indicator) ? 'p' : 'l';

    img.onload = () => {
      width = img.naturalWidth;
      height = img.naturalHeight;

      const pdf = new jsPDF({
        orientation: orientation,
        unit: 'pt',
        format: [width, height],
      });

      pdf.addImage(report, 'png', 0, 0, width, height);
      pdf.save('report.pdf');
    };

    document.body.removeChild(tempContainer);
  }
};

export default makeReport;
