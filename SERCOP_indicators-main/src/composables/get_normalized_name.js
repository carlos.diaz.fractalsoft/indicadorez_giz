const regionsNamesDict = {
  'AZUAY': 'Azuay',
  'BOLIVAR': 'Bolívar',
  'CAÑAR': 'Cañar',
  'CARCHI': 'Carchi',
  'CHIMBORAZO': 'Chimborazo',
  'COTOPAXI': 'Cotopaxi',
  'EL ORO': 'El Oro',
  'ESMERALDAS': 'Esmeraldas',
  'GALAPAGOS': 'Galápagos',
  'GUAYAS': 'Guayas',
  'IMBABURA': 'Imbabura',
  'LOJA': 'Loja',
  'LOS RIOS': 'Los Ríos',
  'MANABI': 'Manabí',
  'MORONA SANTIAGO': 'Morona Santiago',
  'NAPO': 'Napo',
  'ORELLANA': 'Orellana',
  'PASTAZA': 'Pastaza',
  'PICHINCHA': 'Pichincha',
  'SANTA ELENA': 'Santa Elena',
  'SANTO DOMINGO DE LOS TSACHILAS': 'Santo Domingo de los Tsáchilas',
  'SUCUMBIOS': 'Sucumbíos',
  'TUNGURAHUA': 'Tungurahua',
  'ZAMORA CHINCHIPE': 'Zamora Chinchipe',
};

const processesFasesNamesDict = {
  'planificacion': 'Planning (planificación)',
  'licitacion': 'Tender (licitación)',
  'adjudicacion': 'Awards (adjudicación)',
  'contractual': 'Contracts (contractual)',
};

const monthsNamesDict = {
  '0': {'shortName': 'Ene', 'fullName': 'Enero'},
  '1': {'shortName': 'Feb', 'fullName': 'Febrero'},
  '2': {'shortName': 'Mar', 'fullName': 'Marzo'},
  '3': {'shortName': 'Abr', 'fullName': 'Abril'},
  '4': {'shortName': 'May', 'fullName': 'Mayo'},
  '5': {'shortName': 'Jun', 'fullName': 'Junio'},
  '6': {'shortName': 'Jul', 'fullName': 'Julio'},
  '7': {'shortName': 'Ago', 'fullName': 'Agosto'},
  '8': {'shortName': 'Sep', 'fullName': 'Septiembre'},
  '9': {'shortName': 'Oct', 'fullName': 'Octubre'},
  '10': {'shortName': 'Nov', 'fullName': 'Noviembre'},
  '11': {'shortName': 'Dic', 'fullName': 'Diciembre'},
};

const contractingProceduresNamesDict = {
  'asesoria_y_patrocinio_juridico': 'Asesoría y Patrocinio Jurídico',
  'asesoria_y_patrocinio_juridico_cons_puntuales_y_especificas': 'Asesoría y Patrocinio Jurídico – Cons. puntuales y específicas',
  'bienes_y_servicios_unicos': 'Bienes y Servicios únicos',
  'comunicacion_social_contratacion_directa': 'Comunicación Social – Contratación Directa',
  'comunicacion_social_proceso_de_seleccion': 'Comunicación Social – Proceso de Selección',
  'concurso_publico': 'Concurso público',
  'concurso_publico_por_contratacion_desierta': 'Concurso Público por Contratación Directa Desierta',
  'concurso_publico_por_lista_corta_desierta': 'Concurso Público por Lista Corta Desierta',
  'cont_de_instituciones_financieras_y_de_seguros_del_estado': 'Cont. de Instituciones Financieras y de Seguros del Estado',
  'contratacion_de_seguros': 'Contratación de Seguros',
  'contratacion_directa': 'Contratación directa',
  'contratacion_directa_por_terminacion_unilateral': 'Contratación Directa por Terminación Unilateral',
  'contrataciones_con_empresas_publicas_internacionales': 'Contrataciones con empresas públicas internacionales',
  'contratos_entre_entidades_publicas_o_sus_subsidiarias': 'Contratos entre Entidades Públicas o sus subsidiarias',
  'convenio_marco': 'Convenio Marco',
  'cotizacion': 'Cotización',
  'empresas_publicas_mercantiles_o_subsidiarias': 'Empresas Públicas, Mercantiles o Subsidiarias',
  'licitacion': 'Licitación',
  'licitacion_de_seguros': 'Licitación de Seguros',
  'lista_corta': 'Lista corta',
  'lista_corta_por_contratacion_directa_desierta': 'Lista Corta por Contratación Directa Desierta"',
  'menor_cuantia': 'Menor Cuantía',
  'obra_artistica_cientifica_o_literaria': 'Obra artística, científica o literaria',
  'repuestos_o_accesorios': 'Repuestos o Accesorios',
  'sectores_estrategicos': 'Sectores Estratégicos',
  'subasta_inversa_electronica': 'Subasta Inversa Electrónica',
  'transporte_de_correo_interno_o_internacional': 'Transporte de correo interno o internacional',
};

const fasesOfContractingProcedureDict = {
  'licitacion': 'Tender (licitación)',
  'adjudicacion': 'Awards (adjudicación)',
};

const getNormalizedName = function(name, parameter) {

  switch(parameter) {
    case 'region':
      return regionsNamesDict[name];
    case 'process_fase':
      return processesFasesNamesDict[name];
    case 'month_short_name':
      return monthsNamesDict[name]['shortName'];
    case 'month_full_name':
      return monthsNamesDict[name]['fullName'];
    case 'contracting_procedure':
      return contractingProceduresNamesDict[name];
    case 'contracting_procedures_fase':
      return fasesOfContractingProcedureDict[name];
    default:
      return null;
  }

};

export default getNormalizedName;
