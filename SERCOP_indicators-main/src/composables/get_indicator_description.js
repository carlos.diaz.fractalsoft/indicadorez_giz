const descriptions = {
  indicator_1:  'Total de registros que no cuentan con información de monto, en la plataforma de contrataciones abiertas; expresando como porcentaje del total de registros, por fases, de la plataforma de contrataciones abiertas, en un período determinado.',
  indicator_2:  'Total de registros que no cuentan con información de fecha, en la plataforma de contrataciones abiertas; expresado como porcentaje del total de registros, por fases, de la plataforma de contrataciones abiertas, en un  período determinado.',
  indicator_3:  'Total de registros que no cuentan con información de monto, por tipo de procedimiento de contratación; expresado como porcentaje del total de registros por tipo de procedimiento de contratación, en un periodo determinado.',
  indicator_4:  'Total de registros que no cuentan con información de fecha, por tipo de procedimiento de contratación; expresado como porcentaje del total de registros por tipo de procedimiento de contratación, en un periodo determinado.',
  indicator_5:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en monto, por fases del proceso de contratación.',
  indicator_6:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en fecha, por fases del proceso de contratación.',
  indicator_7:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en monto, por fases de proceso de contratación y procedimiento de contratación.',
  indicator_8:  'Listado de las 10 entidades contratantes con mayor cantidad de registros incompletos en fecha, por fases del proceso de contratación y procedimiento de contratación.',
  indicator_9:  'Total de registro de adjudicación donde la diferencia entre la fecha de adjudicación y la fecha actual es mayor a 30 días; expresado como porcentaje del total de registros de adjudicación, en un periodo determinado.',
  indicator_10: 'Sumatoria de los procesos de contratación pública con montos de licitación cercanos a cero.',
  indicator_11: 'Sumatoria de los procesos de contratación publica con montos de adjudicación cercanos a cero.',
  indicator_12: 'Listado de los 10 proveedores con mayor cantidad de registros con monto < $0,5 en fase de licitación.',
  indicator_13: 'Listado de las 10 entidades contratantes con mayor cantidad de registros con montos < $0,5 en la fase de adjudicación.',
  indicator_14: 'Sumatoria de los registros que presentan cambios entre los montos de adjudicación y monto corregido de adjudicación, por tipo de procedimiento de contratación.',
  indicator_15: 'Variabilidad de monto corregido entre monto de adjudicación y monto corregido de adjudicación.',
  indicator_16: 'Gráfico de caja y bigotes para la variable monto licitado, por tipo de procedimiento contratación publica.',
  indicator_17: 'Box plot de monto adjudicado por tipo de procedimiento de contratación pública.',
};

const getIndicatorDescription = function(indicator, toggleSelectedValue) {

  let indicatorDescription = '';

  switch (indicator) {

    case 'indicator_1_2':
      indicatorDescription = toggleSelectedValue === 'monto' ? descriptions['indicator_1'] : descriptions['indicator_2'];
      break;

    case 'indicator_3_4':
      indicatorDescription = toggleSelectedValue === 'monto' ? descriptions['indicator_3'] : descriptions['indicator_4'];
      break;

    case 'indicator_5_6':
      indicatorDescription = toggleSelectedValue === 'monto' ? descriptions['indicator_5'] : descriptions['indicator_6'];
      break;

    case 'indicator_7_8':
      indicatorDescription = toggleSelectedValue === 'monto' ? descriptions['indicator_7'] : descriptions['indicator_8'];
      break;

    case 'indicator_10_11':
      indicatorDescription = toggleSelectedValue === 'licitacion' ? descriptions['indicator_10'] : descriptions['indicator_11'];
      break;

    case 'indicator_9':
    case 'indicator_12':
    case 'indicator_13':
    case 'indicator_14':
    case 'indicator_15':
    case 'indicator_16':
    case 'indicator_17':
      indicatorDescription = descriptions[indicator];
      break;

    default:
      indicatorDescription = undefined;
      break;
  }

  return indicatorDescription;
}

export default getIndicatorDescription;
