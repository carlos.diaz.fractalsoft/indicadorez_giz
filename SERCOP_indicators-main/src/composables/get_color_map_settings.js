const colorMapSettings = {
  'indicator_1_2':    {dataUnit: '%', isInteger: false, mapLegendText: 'Incompleto'},
  'indicator_3_4':    {dataUnit: '%', isInteger: false, mapLegendText: 'Incompleto'},
  'indicator_5_6':    {dataUnit: 'registros', isInteger: true, mapLegendText: 'Número de registros incompletos'},
  'indicator_7_8':    {dataUnit: 'registros', isInteger: true, mapLegendText: 'Número de registros incompletos'},
  'indicator_9':      {dataUnit: '%', isInteger: false, mapLegendText: 'Porcentaje de registros por días'},
  'indicator_10_11':  {dataUnit: 'registros', isInteger: true, mapLegendText: 'Número registros'},
  'indicator_12':     {dataUnit: 'registros', isInteger: true, mapLegendText: 'Número de registros con monto < $0.5'},
  'indicator_13':     {dataUnit: 'registros', isInteger: true, mapLegendText: 'Número de registros con monto < $0.5'},
  'indicator_14':     {dataUnit: 'registros', isInteger: true, mapLegendText: 'Número registros'},
  'indicator_15':     {dataUnit: '$', isInteger: false, mapLegendText: 'Variabilidad en monto'},
};

const getColorMapSettings = (indicator) => colorMapSettings[indicator];

export default getColorMapSettings;
