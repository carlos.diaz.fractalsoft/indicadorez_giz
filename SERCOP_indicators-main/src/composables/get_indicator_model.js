import { reactive } from "vue";

const paramsInformacionIncompleta = [
  'monto',
  'fecha',
];

const processFases = [
  'planificacion',
  'licitacion',
  'adjudicacion',
  'contractual',
];

const provinciasDefaultSelect = [
  "AZUAY",
  "BOLIVAR",
  "CAÑAR",
  "CARCHI",
  "CHIMBORAZO",
  "COTOPAXI",
  "EL ORO",
  "ESMERALDAS",
  "GALAPAGOS",
  "GUAYAS",
  "IMBABURA",
  "LOJA",
  "LOS RIOS",
  "MANABI",
  "MORONA SANTIAGO",
  "NAPO",
  "ORELLANA",
  "PASTAZA",
  "PICHINCHA",
  "SANTA ELENA",
  "SANTO DOMINGO DE LOS TSACHILAS",
  "SUCUMBIOS",
  "TUNGURAHUA",
  "ZAMORA CHINCHIPE",
];

const contractingProcedures = [
  'asesoria_y_patrocinio_juridico',
  'asesoria_y_patrocinio_juridico_cons_puntuales_y_especificas',
  'bienes_y_servicios_unicos',
  'comunicacion_social_contratacion_directa',
  'comunicacion_social_proceso_de_seleccion',
  'concurso_publico',
  'concurso_publico_por_contratacion_desierta',
  'concurso_publico_por_lista_corta_desierta',
  'cont_de_instituciones_financieras_y_de_seguros_del_estado',
  'contratacion_de_seguros',
  'contratacion_directa',
  'contratacion_directa_por_terminacion_unilateral',
  'contrataciones_con_empresas_publicas_internacionales',
  'contratos_entre_entidades_publicas_o_sus_subsidiarias',
  'convenio_marco',
  'cotizacion',
  'empresas_publicas_mercantiles_o_subsidiarias',
  'licitacion',
  'licitacion_de_seguros',
  'lista_corta',
  'lista_corta_por_contratacion_directa_desierta',
  'menor_cuantia',
  'obra_artistica_cientifica_o_literaria',
  'repuestos_o_accesorios',
  'sectores_estrategicos',
  'subasta_inversa_electronica',
  'transporte_de_correo_interno_o_internacional',
];

const faseProcedimientoContratacion = [
  'licitacion',
  'adjudicacion',
];

const getLastPassedYearMonth = () => {
    const today = new Date();
    let lastMonthYear, lastMonthMonth;

    if (today.getMonth() === 0) {
      lastMonthYear = today.getFullYear() - 1;
      lastMonthMonth = 11;
    } else {
      lastMonthYear = today.getFullYear();
      lastMonthMonth = today.getMonth() - 1;
    }

    return [lastMonthYear, lastMonthMonth];
  }

const composeUserDateRangeQuery = function(startYear, startMonth, lastYear, lastMonth) {
  return {
    userDateQueryStart: {
      pickedYear: startYear,
      pickedMonth: startMonth,
    },
    userDateQueryEnd: {
      pickedYear: lastYear,
      pickedMonth: lastMonth,
    },
  };
};

const getIndicatorModel = function(indicator) {

  let indicatorModel = {};
  const lastPassedYearMonth = getLastPassedYearMonth();

  switch (indicator) {

    case 'indicator_1_2':
      indicatorModel = {
        toggleSelectedValue: paramsInformacionIncompleta[0],
        singleChoiseSelectedValue: processFases[0],
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2019, 2, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;

    case 'indicator_3_4':
      indicatorModel = {
        toggleSelectedValue: paramsInformacionIncompleta[0],
        singleChoiseSelectedValue: contractingProcedures[19],
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2018, 2, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;

    case 'indicator_5_6':
      indicatorModel = {
        toggleSelectedValue: paramsInformacionIncompleta[0],
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2015, 0, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;

    case 'indicator_7_8':
      indicatorModel = {
        toggleSelectedValue: paramsInformacionIncompleta[0],
        singleChoiseSelectedValue: contractingProcedures[19],
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2017, 0, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;

    case 'indicator_9':
      indicatorModel = {
        singleChoiseSelectedValue: contractingProcedures[19],
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2018, 0, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;

    case 'indicator_10_11':
      indicatorModel = {
        toggleSelectedValue: faseProcedimientoContratacion[0],
        singleChoiseSelectedValue: contractingProcedures[19],
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2019, 2, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;



    case 'indicator_12':
    case 'indicator_13':
      indicatorModel = {
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2018, 0, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;



    case 'indicator_14':
    case 'indicator_15':
      indicatorModel = {
        singleChoiseSelectedValue: contractingProcedures[19],
        provinciasSelectedValues: provinciasDefaultSelect,
        userDateRangeQuery: composeUserDateRangeQuery(2019, 0, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;


      case 'indicator_16':

    case 'indicator_17':
      indicatorModel = {
        singleChoiseSelectedValue: contractingProcedures[19],
        userDateRangeQuery: composeUserDateRangeQuery(2019, 0, lastPassedYearMonth[0], lastPassedYearMonth[1]),
        isUserQueryValid: true,
      }
      break;
  }

  return reactive(indicatorModel);
};

export default getIndicatorModel;
