const titles = {
  indicator_1_2:    'Porcentaje de registros con información incompleta',
  indicator_3_4:    'Porcentaje de registros con información incompleta por tipo de procedimiento de contratación en la fase de adjudicación',
  indicator_5_6:    'Ranking de entidades contratantes con mayor cantidad de registros incompletos',
  indicator_7_8:    'Ranking de entidades contratantes con mayor cantidad de registros incompletos por procedimiento de contratación',
  indicator_9:      'Contraste de registros de fecha de adjudicación vs fecha actual medida en días',
  indicator_10_11:  'Número de registros con datos de montos $0 y $0,5',
  indicator_12:     'Ranking de proveedores por mayor cantidad de registros con monto < $0,5 en la fase de licitación',
  indicator_13:     'Ranking de entidades contratantes por mayor cantidad de registros con monto < $0,5 en la fase de adjudicación',
  indicator_14:     'Registros con diferencias entre el monto de adjudicación y monto corregido por tipo de procedimiento de contratación',
  indicator_15:     'Variabilidad de monto corregido entre monto de adjudicación y monto corregido de adjudicación',
  indicator_16:     'Box plot de monto licitado por tipo de procedimiento de contratación pública',
  indicator_17:     'Box plot de monto adjudicado por tipo de procedimiento de contratación pública',
};

const getCurrentIndicatorTitle = (indicator) => titles[indicator];

export default getCurrentIndicatorTitle;
