import * as XLSX from 'xlsx';
import getCurrentIndicatorTitle from '../composables/get_indicator_title';
import getNormalizedName from './get_normalized_name';

const formatDateString = (dateString) => {
  const year = dateString.split(".")[0];
  const month = parseInt(dateString.split(".")[1]) - 1;
  const monthName = getNormalizedName(month, 'month_short_name');
  return `${monthName} ${year}`;
}

const downloadDataCSV = (indicator, chartData) => {

  const header = {
    'indicator_1_2': 'Fecha,Porcentaje (%) de registros con información incompleta\n',
    'indicator_3_4': 'Fecha,Porcentaje (%) de registros con información incompleta\n',
    'indicator_5_6': 'Entidad Contratante,Número de registros incompletos\n',
    'indicator_7_8': 'Entidad Contratante,Número de registros incompletos\n',
    'indicator_9': 'Fecha,Porcentaje de registros donde la diferencia entre la fecha de adjudicación y la fecha actual es mayor a 30 días\n',
    'indicator_10_11': 'Fecha,Número de registros de contratación pública con montos de adjudicación con valores cercanos a $0\n',
    'indicator_12': 'Entidad Contratante,Número de registros con monto < $0.5\n',
    'indicator_13': 'Entidad Contratante,Número de registros con monto < $0.5\n',
    'indicator_14': 'Fecha,Número de registros donde el monto adjudicado ha sido corregido por el SERCOP\n',
    'indicator_15': 'Fecha,Variabilidad en monto adjudicado corregido por el SERCOP\n',
    'indicator_16': 'Fecha,Monto Licitado\n',
    'indicator_17': 'Fecha,Monto Adjudicado\n',
  }[indicator];

  let csvData = [];

  if (!['indicator_16', 'indicator_17'].includes(indicator)) {
    csvData = chartData.map(item => {
      switch (indicator) {
        case 'indicator_1_2':
        case 'indicator_3_4':
          return [formatDateString(item.yearmonth), item.porcentaje];
        case 'indicator_5_6':
        case 'indicator_7_8':
          return [item.entidad_contratante, item.register_number];
        case 'indicator_9':
          return [formatDateString(item.yearmonth), item.porcentaje];
        case 'indicator_10_11':
          return [formatDateString(item.yearmonth), item.register_number];
        case 'indicator_12':
        case 'indicator_13':
          return [item.entidad_contratante, item.register_number];
        case 'indicator_14':
          return [formatDateString(item.yearmonth), item.register_number];
        case 'indicator_15':
          return [formatDateString(item.yearmonth), item.variabilidad];
        default:
          return null;
      }
    }).filter(item => item !== null);
  } else {
    csvData = chartData.reduce((acc, item) => {
      let key = Object.keys(item).map(elem => formatDateString(elem))[0];
      let valuesList = Object.values(item)[0]; 
      for (let value of valuesList) {
        acc.push([key, value.toString()]);
      }
      return acc;
    }, []);
  }

  const csv = `${header}${csvData.map(row => row.join(',')).join('\n')}`;
  const blob = new Blob([csv], { type: 'text/csv' });
  const url = URL.createObjectURL(blob);
  const link = document.createElement('a');
  const indicatorTitle = getCurrentIndicatorTitle(indicator);
  link.href = url;
  link.download = `${indicatorTitle.toLowerCase().replace(/\s+/g, '_')}_data.csv`;
  link.click();
};

function getMaxLengths(arr) {
  return arr.reduce((acc, curr) => {
    curr.forEach((el, i) => {
      if (typeof el === 'string' || typeof el === 'number') {
        const str = String(el);
        acc[i] = Math.max(acc[i] || 0, str.length);
      }
    });
    return acc;
  }, []);
}

const downloadDataXLSX = (indicator, chartData) => {

  const header = {
    'indicator_1_2': ['Fecha', 'Porcentaje (%) de registros con información incompleta'],
    'indicator_3_4': ['Fecha', 'Porcentaje (%) de registros con información incompleta'],
    'indicator_5_6': ['Entidad Contratante', 'Número de registros incompletos'],
    'indicator_7_8': ['Entidad Contratante', 'Número de registros incompletos'],
    'indicator_9': ['Fecha', 'Porcentaje de registros donde la diferencia entre la fecha de adjudicación y la fecha actual es mayor a 30 días'],
    'indicator_10_11': ['Fecha', 'Número de registros de contratación pública con montos de adjudicación con valores cercanos a $0'],
    'indicator_12': ['Entidad Contratante', 'Número de registros con monto < $0.5'],
    'indicator_13': ['Entidad Contratante', 'Número de registros con monto < $0.5'],
    'indicator_14': ['Fecha', 'Número de registros donde el monto adjudicado ha sido corregido por el SERCOP'],
    'indicator_15': ['Fecha', 'Variabilidad en monto adjudicado corregido por el SERCOP'],
    'indicator_16': ['Fecha', 'Monto Licitado'],
    'indicator_17': ['Fecha', 'Monto Adjudicado']
  }[indicator];

  // Map the data to a 2D array

  let dataAoa = [];

  if (!['indicator_16', 'indicator_17'].includes(indicator)) {
    dataAoa = chartData.map(item => {
      switch (indicator) {
        case 'indicator_1_2':
        case 'indicator_3_4':
          return [formatDateString(item.yearmonth), item.porcentaje];
        case 'indicator_5_6':
        case 'indicator_7_8':
          return [item.entidad_contratante, item.register_number];
        case 'indicator_9':
          return [formatDateString(item.yearmonth), item.porcentaje];
        case 'indicator_10_11':
          return [formatDateString(item.yearmonth), item.register_number];
        case 'indicator_12':
        case 'indicator_13':
          return [item.entidad_contratante, item.register_number];
        case 'indicator_14':
          return [formatDateString(item.yearmonth), item.register_number];
        case 'indicator_15':
          return [formatDateString(item.yearmonth), item.variabilidad];
        default:
          return null;
      }
    });
  } else {
    dataAoa = chartData.reduce((acc, item) => {
      let key = Object.keys(item).map(elem => formatDateString(elem))[0];
      let valuesList = Object.values(item)[0];
      for (let value of valuesList) {
        acc.push([key, value.toString()])
      }
      return acc;
    }, []);
  }

  console.log(dataAoa);
  

  // add a header to dataset
  dataAoa.unshift(header);

  // create empty workbook
  const workbook = XLSX.utils.book_new();

  // add new sheet to workbook
  const sheetName = 'Sheet1';
  const sheet = XLSX.utils.aoa_to_sheet([[]]);
  XLSX.utils.book_append_sheet(workbook, sheet, sheetName);

  // add data to worksheet
  XLSX.utils.sheet_add_aoa(sheet, dataAoa, { origin: 'A1' });

  // calculate and assign appropriate width to every colemn of worksheet
  let maxLengths = getMaxLengths(dataAoa);
  sheet["!cols"] = maxLengths.map(elem => Object.assign( {wch: elem} ) );

  const indicatorTitle = getCurrentIndicatorTitle(indicator);

  // Write the workbook to a file
  XLSX.writeFile(workbook, `${indicatorTitle.toLowerCase().replace(/\s+/g, '_')}_data.xlsx`);

}

export { downloadDataCSV, downloadDataXLSX };