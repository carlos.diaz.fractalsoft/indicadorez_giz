const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', redirect: '/completitud' },
      { path: '/completitud', component: () => import('src/pages/CompletitudPage.vue') },
      { path: '/oportunidad', component: () => import('src/pages/OportunidadPage.vue') },
      { path: '/exactitud', component: () => import('src/pages/ExactitudPage.vue') },
      { path: '/precision', component: () => import('src/pages/PrecisionPage.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
